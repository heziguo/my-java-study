package service;

/**
 * Title: service.HelloService<br/>
 * Description: <br/>
 * version 1.0<br/>
 * author hzg<br/>
 * date 2021/9/13 14:56<br/>
 * @author Administrator
 */
public interface HelloService {
    /**
     * Description: 说话<br/>
     * author: hzg <br/>
     * Date: 2021/9/13 14:57 <br/>
     * @param content
     * @return java.lang.String
     */
    String sayHello(String content);
}
