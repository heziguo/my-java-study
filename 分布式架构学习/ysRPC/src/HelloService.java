import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Title: PACKAGE_NAME.HelloService<br/>
 * Description: <br/>
 * @version 1.0<br/>
 * @author hzg<br/>
 * @date 2021/9/17 10:32<br/>
 */
public interface HelloService extends Remote {
    /**
     * Description: <br/>
     * author: hzg <br/>
     * Date: 2021/9/17 10:35 <br/>
     * @param someOne 名称
     * @return java.lang.String
     */
    String sayHello(String someOne) throws RemoteException;
}
