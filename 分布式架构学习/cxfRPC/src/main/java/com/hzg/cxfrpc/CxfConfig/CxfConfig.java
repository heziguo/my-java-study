package com.hzg.cxfrpc.CxfConfig;

import com.hzg.cxfrpc.service.HelloService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * Title: com.hzg.cxfrpc.CxfConfig.CxfConfig<br/>
 * Description: 配置<br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/17 13:59<br/>
 */
@Configuration
public class CxfConfig {

    @Autowired
    private Bus bus;

    @Autowired
    private HelloService helloService;

    @Bean
    public Endpoint endpoint(){
        EndpointImpl endpoint = new EndpointImpl(bus, helloService);
        endpoint.publish("/HelloService");
        return endpoint;
    }
}
