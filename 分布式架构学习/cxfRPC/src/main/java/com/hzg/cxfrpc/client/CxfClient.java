package com.hzg.cxfrpc.client;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

/**
 * Title: com.hzg.cxfrpc.client.CxfClient<br/>
 * Description: 客户端调用<br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/17 14:21<br/>
 */
public class CxfClient {
    public static void main(String[] args) throws Exception{
        //创建动态客户端
        JaxWsDynamicClientFactory jaxWsDynamicClientFactory = JaxWsDynamicClientFactory.newInstance();
        Client client = jaxWsDynamicClientFactory.createClient("http://localhost:8080/services/HelloService?wsdl");
        //需要密码的情况需要加上用户名和密码
        //client.getOutFaultInterceptors().add(new ClientLoginInterceptor(USER_NAME,PASS_WORD));
        Object[] objects = new Object[0];
        //invoke("方法名",参数1，参数2...)；
        objects = client.invoke("sayHello","hzg");
        System.out.println("返回数据:"+objects[0]);
    }
}
