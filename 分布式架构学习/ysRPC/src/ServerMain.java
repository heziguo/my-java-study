import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.RMISocketFactory;

/**
 * Title: PACKAGE_NAME.ServerMain<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/17 10:40<br/>
 */
public class ServerMain {

    public static void main(String[] args) throws Exception{
        //注册服务
        LocateRegistry.createRegistry(8801);
        RMISocketFactory.setSocketFactory(new CustomerSocketFactory());
        //创建服务
        HelloService helloService = new HelloServiceImpl();
        Naming.bind("rmi://192.168.1.117:8801/helloService",helloService);
        System.out.println("服务端 RPC 启动成功!!");
    }
}
