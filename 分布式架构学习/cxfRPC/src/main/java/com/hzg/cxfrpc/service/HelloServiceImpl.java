package com.hzg.cxfrpc.service;

import org.springframework.stereotype.Component;

import javax.jws.WebService;

/**
 * Title: com.hzg.cxfrpc.service.HelloServiceImpl<br/>
 * Description: <br/>
 * version: 1.0<br/>
 * 访问地址： http://localhost:8080/services/HelloService?wsdl
 * @author : hzg<br/>
 * date: 2021/9/17 11:30<br/>
 */
@WebService(serviceName = "HelloService", //与接口指定的名称一致
        targetNamespace = "http://service.cxfrpc.hzg.com",//与接口中的命名空间一致
        endpointInterface = "com.hzg.cxfrpc.service.HelloService"//接口地址
)
@Component
public class HelloServiceImpl implements HelloService{
    /**
     * Description: <br/>
     * author: hzg <br/>
     * Date: 2021/9/17 11:30 <br/>
     *
     * @param content 内容
     * @return java.lang.String
     */

    @Override
    public String sayHello(String content) {
        return "hello,"+content;
    }
}
