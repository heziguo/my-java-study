import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.RMISocketFactory;

/**
 * Title: PACKAGE_NAME.CustomerSocketFactory<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/17 10:57<br/>
 */
public class CustomerSocketFactory extends RMISocketFactory {
    /**
     * Creates a client socket connected to the specified host and port.
     * 指定通信端口防止被防火墙拦截
     * @param host the host name
     * @param port the port number
     * @return a socket connected to the specified host and port.
     * @throws IOException if an I/O error occurs during socket creation
     * @since 1.1
     */
    @Override
    public Socket createSocket(String host, int port) throws IOException {
        return new Socket(host,port);
    }

    /**
     * Create a server socket on the specified port (port 0 indicates
     * an anonymous port).
     *
     * @param port the port number
     * @return the server socket on the specified port
     * @throws IOException if an I/O error occurs during server socket
     *                     creation
     * @since 1.1
     */
    @Override
    public ServerSocket createServerSocket(int port) throws IOException {
        if (port == 0){
            port = 8501;
        }
        System.out.println("rmi notify port: "+port);
        return new ServerSocket(port);
    }
}
