package invoke;

import framework.ProviderReflect;
import service.HelloService;
import service.HelloServiceImpl;

/**
 * Title: invoke.RpcProviderMain<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/13 15:27<br/>
 */
public class RpcProviderMain {
    public static void main(String[] args) throws Exception{
        HelloService service = new HelloServiceImpl();
        ProviderReflect.provider(service,8083);
    }
}
