package service;

/**
 * Title: service.HelloServiceImpl<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/13 14:58<br/>
 */
public class HelloServiceImpl implements HelloService{
    /**
     * Description: 说话<br/>
     * author: hzg <br/>
     * Date: 2021/9/13 14:57 <br/>
     *
     * @param content
     * @return java.lang.String
     */
    @Override
    public String sayHello(String content) {
        return "hello,"+content;
    }
}
