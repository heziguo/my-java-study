package com.hzg.tensquare.base.service;

import com.hzg.tensquare.base.dao.LabelDao;
import com.hzg.tensquare.base.pojo.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import util.IdWorker;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Title: com.hzg.tensquare.base.service.LabelService<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/22 10:57<br/>
 */
@Service
@Transactional
public class LabelService {
    @Autowired
    private LabelDao labelDao;
    @Autowired
    private IdWorker idWorker;

    public List<Label> findAll(){
        return labelDao.findAll();
    }

    public Label findById(String id){
        return labelDao.findById(id).get();
    }

    public void save(Label label){
        label.setId(idWorker.nextId()+"");
        labelDao.save(label);
    }

    public void update(Label label){
        labelDao.save(label);
    }

    public void deleteById(String id){
        labelDao.deleteById(id);
    }

    public List<Label> findSearch(Label label){
        return labelDao.findAll((Specification<Label>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list =  new ArrayList<>();
            if (label.getLabelname()!=null && !"".equals(label.getLabelname())){
                Predicate labelName = criteriaBuilder.like(root.get("labelname").as(String.class), "%" + label.getLabelname() + "%");
                list.add(labelName);
            }
            if (label.getState()!=null && !"".equals(label.getState())){
                Predicate state = criteriaBuilder.equal(root.get("state").as(String.class), label.getState());
                list.add(state);
            }
            Predicate[] parr = new Predicate[list.size()];
            list.toArray(parr);
            return criteriaBuilder.and(parr);
        });
    }

    public Page<Label> pageQuery(int page, int size, Label label) {
        Pageable pageable = PageRequest.of(page-1,size);
        return labelDao.findAll((Specification<Label>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list =  new ArrayList<>();
            if (label.getLabelname()!=null && !"".equals(label.getLabelname())){
                Predicate labelName = criteriaBuilder.like(root.get("labelname").as(String.class), "%" + label.getLabelname() + "%");
                list.add(labelName);
            }
            if (label.getState()!=null && !"".equals(label.getState())){
                Predicate state = criteriaBuilder.equal(root.get("state").as(String.class), label.getState());
                list.add(state);
            }
            Predicate[] parr = new Predicate[list.size()];
            list.toArray(parr);
            return criteriaBuilder.and(parr);
        },pageable);
    }
}
