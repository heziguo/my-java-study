import java.io.File;
import java.io.FileFilter;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Title: PACKAGE_NAME.Lambda<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/1 14:41<br/>
 */
public class Lambda {
    public static void main(String[] args) {
        File[] files = new File(".").listFiles(File::isHidden);
        System.out.println(Arrays.toString(files));

        process(()-> System.out.println("abc"));
        process(() -> System.out.println("sss"));
        Predicate<String> ps =  s->!s.isEmpty();
        ps =ps.negate();
        List<String> filter = filter(Arrays.asList("2", "", "3", ""), ps);
        System.out.println(filter);
        forEach(Arrays.asList(1,3,2,6,4),p -> System.out.println(p));
        Map<String, Integer> stringIntegerMap = listToMap(Arrays.asList(1, 3, 2, 6, 4), p -> p);
        System.out.println(stringIntegerMap);

       IntStream.rangeClosed(1, 100).boxed().flatMap(a->IntStream.rangeClosed(a,100)
               .filter(b->Math.sqrt(a*a+b*b) % 1 ==0)
               .mapToObj(b->new int[]{a,b,(int)Math.sqrt(a*a+b*b)})
       ).forEach(t -> System.out.println(t[0] + ", " + t[1] + ", " + t[2]));


       Stream.iterate(new int[]{0, 1}, n->new int[]{n[1],n[0]+n[1]}).limit(20).forEach(t -> System.out.println(t[0] + ", " + t[1]));
    }

    public static void process(Runnable runnable){
        runnable.run();
    }

    //Predicate : T->boolean
    public static <T> List<T> filter (List<T> list, Predicate<T> p){
        List<T> result = new ArrayList<>();
        for (T t : list){
            if (p.test(t)){
                result.add(t);
            }
        }
        return  result;
    }
    //Consumer: t->void
    public static <T> void forEach(List<T> list, Consumer<T> p){
        for (T t : list){
            p.accept(t);
        }
    }

    //FUnction:<T> -><R>
    public static <T,R> Map<String,R> listToMap(List<T> list, Function<T,R> p){
        Map<String,R> map = new HashMap<>();
        for (T t : list){
            map.put(t.toString(),p.apply(t));
        }
        return map;
    }
}
