package com.hzg.cxfrpc.service;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
/**
 * Title: com.hzg.cxfrpc.service.HelloService<br/>
 * Description: <br/>
 * version 1.0<br/>
 * @author hzg<br/>
 * date 2021/9/17 11:29<br/>
 */
@WebService(name="HelloService", //暴露服务名称
        targetNamespace = "http://service.cxfrpc.hzg.com" //命名空间，一般是接口的包名倒序
)
public interface HelloService {
    /**
     * Description: <br/>
     * author: hzg <br/>
     * Date: 2021/9/17 11:30 <br/> 
     * @param content 内容
     * @return java.lang.String
     */
    @WebMethod
    @WebResult(name="String",targetNamespace = "")
    String sayHello(String content);
}
