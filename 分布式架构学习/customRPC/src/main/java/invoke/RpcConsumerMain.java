package invoke;

import framework.ConsumerProxy;
import service.HelloService;

/**
 * Title: invoke.RpcConsumerMain<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/13 15:29<br/>
 */
public class RpcConsumerMain {
    public static void main(String[] args) throws Exception{
        HelloService service = ConsumerProxy.consume(HelloService.class, "127.0.0.1", 8083);
        for (int i = 0; i < 1000; i++) {
            String hello = service.sayHello("heziguo_"+i);
            System.out.println(hello);
            Thread.sleep(1000);
        }
    }
}
