package entity;

import java.util.List;

/**
 * Title: entity.PageResult<br/>
 * Description: 分页实体<br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/18 15:37<br/>
 */
public class PageResult<T> {
    private long total;
    private List<T> rows;

    public PageResult() {
    }

    public PageResult(long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}
