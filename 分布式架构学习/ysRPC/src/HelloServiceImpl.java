import java.io.Serial;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Title: PACKAGE_NAME.HelloServiceImpl<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/17 10:35<br/>
 */
public class HelloServiceImpl extends UnicastRemoteObject implements HelloService {
    @Serial
    private static final long serialVersionUID = -6190523770400890033L;
    /**
     * Creates and exports a new UnicastRemoteObject object using an
     * anonymous port.
     *
     * <p>The object is exported with a server socket
     * created using the {@link RMISocketFactory} class.
     *
     * @throws RemoteException if failed to export object
     * @since 1.1
     */
    public HelloServiceImpl() throws RemoteException {
        super();
    }

    /**
     * Description: <br/>
     * author: hzg <br/>
     * Date: 2021/9/17 10:35 <br/>
     *
     * @param someOne 名称
     * @return java.lang.String
     */
    @Override
    public String sayHello(String someOne) throws RemoteException {
        return "Hello,"+someOne;
    }
}
