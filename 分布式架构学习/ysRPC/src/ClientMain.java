import java.rmi.Naming;
import java.rmi.Remote;

/**
 * Title: PACKAGE_NAME.Client<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/17 10:45<br/>
 */
public class ClientMain {
    public static void main(String[] args) throws Exception{
        //服务引用
        HelloService helloService = (HelloService)Naming.lookup("rmi://192.168.1.117:8801/helloService");
        //调用远程方法
        System.out.println("RMI 服务器返回的结果是: "+helloService.sayHello("hzg"));
    }
}
