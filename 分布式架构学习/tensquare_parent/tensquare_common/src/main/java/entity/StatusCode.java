package entity;

/**
 * Title: entity.StatusCode<br/>
 * Description: 状态码实体类<br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/18 15:39<br/>
 */
public class StatusCode {
    /**
     * 成功
     */
    public static final long OK=20000;
    /**
     * 失败
     */
    public static final long ERROR =20001;
    /**
     * 用户名或密码错误
     */
    public static final long LOGINERROR =20002;
    /**
     * 权限不足
     */
    public static final long ACCESSERROR =20003;
    /**
     * 远程调用失败
     */
    public static final long REMOTEERROR =20004;
    /**
     * 重复操作
     */
    public static final long REPERROR =20005;
}
