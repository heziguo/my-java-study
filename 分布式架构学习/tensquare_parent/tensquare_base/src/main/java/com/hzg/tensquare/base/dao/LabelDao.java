package com.hzg.tensquare.base.dao;

import com.hzg.tensquare.base.pojo.Label;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Title: com.hzg.tensquare.base.dao.LabelDao<br/>
 * Description: <br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/22 10:18<br/>
 */
public interface LabelDao extends JpaRepository<Label,String>, JpaSpecificationExecutor<Label> {

}
