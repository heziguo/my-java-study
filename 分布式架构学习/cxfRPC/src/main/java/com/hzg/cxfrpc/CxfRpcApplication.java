package com.hzg.cxfrpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CxfRpcApplication {

    public static void main(String[] args) {
        SpringApplication.run(CxfRpcApplication.class, args);
    }

}
