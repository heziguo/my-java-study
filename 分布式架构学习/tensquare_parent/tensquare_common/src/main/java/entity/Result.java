package entity;

/**
 * Title: entity.Result<br/>
 * Description: 返回页面的数据类<br/>
 * version: 1.0<br/>
 *
 * @author : hzg<br/>
 * date: 2021/9/18 15:32<br/>
 */
public class Result {
    private Boolean flag;
    private Long code;
    private String message;
    private Object data;


    public Result() {
    }

    public Result(Boolean flag, Long code, String message) {
        this.flag = flag;
        this.code = code;
        this.message = message;
    }

    public Result(Boolean flag, Long code, String message, Object data) {
        this.flag = flag;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
